import React from "react";
import Image from "next/dist/client/image";
import Link from "@/components/UI/atoms/LinkComponent";

const MonthlyCharges: React.FC<{ value: number }> = (props) => {
  const { value = 0.0 } = props;
  return (
    <>
      <div className="flex flex-wrap">
        {/* <div className="py-6 w-full lg:w-1/2 pl-9">
          <p className="text-md text-payline-black mb-3">Card Brand:</p>
          <h3 className="text-3xl text-payline-black font-bold">
            <span>$</span>
            <span className="value">0.00</span>
          </h3>
        </div> */}
        <div className="pt-6 pb-8 w-full px-9">
          <p className="text-md text-payline-black ">Including:</p>
          <div className="flex gap-2 mb-3">
            <Image src="/images/svg/visa.svg" width={49} height={16} />
            <Image src="/images/svg/mastercard.svg" width={34} height={26} />
            <Image src="/images/svg/discover.svg" width={74} height={41} />
            <Image src="/images/svg/amex.svg" width={31} height={31} />
          </div>
          <Link
            href="/pricing/#interchangeRate"
            className="underline text-payline-black">
            See interchange pricing
          </Link>
        </div>
        <div className="w-full border-t-2 border-dashed border-payline-border-light px-9 pt-4">
          <p className="text-lg font-hkgrotesk text-payline-black">Total:</p>
          <h3 className="text-4xl text-payline-black font-bold">
            <span>$</span>
            <span className="value">{value.toFixed(2)}</span>
          </h3>
        </div>
      </div>
    </>
  );
};

export default MonthlyCharges;
