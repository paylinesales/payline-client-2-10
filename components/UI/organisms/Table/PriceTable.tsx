import React from "react";

type columnProps = {
  title: string;
  data: Array<string>;
  colNumber: number;
};

type columnData = {
  title: string;
  data: Array<string>;
};

type tableProps = {
  data: Array<columnData>;
};

const TableColumn: React.FC<columnProps> = ({ title, data, colNumber }) => {
  const firstColumn = Boolean(colNumber === 0);
  const secondColumn = Boolean(colNumber === 1);
  const lastColumn = Boolean(colNumber === 5);

  // used constants to change classes because tailwind variants aren't working at the moment
  return (
    <div
      className={`${
        secondColumn
          ? "filter drop-shadow-2xl -mt-3 -mb-3 rounded border border-payline-border-light"
          : ""
      } border-payline-border-light ${
        firstColumn ? "rounded-tl-md rounded-bl-md border-l" : ""
      } ${
        lastColumn ? "rounded-tr-md rounded-br-md border-r" : ""
      }  border-t border-b`}>
      <div
        className={`${
          secondColumn
            ? "bg-payline-cta text-payline-white font-bold overflow-hidden"
            : "min-h-2.5"
        } flex justify-center flex-grow p-2  h-14 lg:h-10`}>
        {title}
      </div>
      <div>
        {data.map((item, i) => {
          const evenItem = Boolean(i % 2 === 0);

          return (
            <div
              // eslint-disable-next-line react/no-array-index-key
              key={i}
              className={`${
                evenItem ? "bg-payline-white" : "bg-payline-background-light"
              } flex p-2 px-8 min-w-max ${
                firstColumn ? "justify-start px-5" : "justify-center"
              } ${secondColumn ? "font-bold h-10.5" : ""}`}>
              {item}
            </div>
          );
        })}
      </div>
    </div>
  );
};

const PriceTable: React.FC<tableProps> = ({ data }) => {
  return (
    <div className="contentWrapper flex justify-center my-20">
      {data.map((item, i) => {
        // eslint-disable-next-line react/no-array-index-key
        return <TableColumn {...item} key={i} colNumber={i} />;
      })}
    </div>
  );
};

export default PriceTable;
