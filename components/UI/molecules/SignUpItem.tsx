import React, { ReactElement } from "react";
// import { Url } from "url";

interface Props {
  image: {
    sourceUrl: string;
    altText: string;
  };
  label: string;
  link: {
    url: string;
    title: string;
  };
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
function SignUpItem({
  label,
  link: { url, title },
  image: { sourceUrl, altText },
}: Props): ReactElement {
  return (
    <div className="flex flex-row gap-8 my-8 ">
      <div>
        <img src={sourceUrl} alt={altText} />
      </div>
      <a href={url} className="flex flex-row">
        <div>
          <p className="font-bold">{label}</p>
          {title}
        </div>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-6 w-6 ml-6 place-self-center"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor">
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M9 5l7 7-7 7"
          />
        </svg>
      </a>
    </div>
  );
}

export default SignUpItem;
