import React from "react";
import { useBusinessCategory } from "context/BusinessCategoryContext";
import { PAYMENT_TYPES } from "../../../constants";

const { ONLINE } = PAYMENT_TYPES;
const ColorTextBox: React.FC<{ className?: string }> = (props) => {
  const { className: classes = "", children } = props;
  const { paymentType } = useBusinessCategory();

  // for as long as the payment type is on line then the business is online
  const isOnlineBusiness = paymentType === ONLINE;

  return (
    <span
      className={`${
        isOnlineBusiness ? "bg-payline-green" : "bg-payline-blue"
      } mx-2 text-payline-white px-2 py-1 ${classes}`}>
      {children}
    </span>
  );
};

ColorTextBox.defaultProps = {
  className: "",
};

export default ColorTextBox;
