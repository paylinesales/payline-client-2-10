/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable valid-typeof */
/* eslint-disable react/no-array-index-key */
import * as React from "react";

interface Props {
  content: React.ReactNode | any;
}

const AccordionItem: React.FC<Props> = ({ content }) => {
  const accordionItems: any[] = [];

  // eslint-disable-next-line array-callback-return
  if (typeof content !== "string") {
    content?.forEach((value, index) => {
      const { itemDetailsText, priceDetails } = value;
      accordionItems.push(
        <div
          className=" rounded md:rounded-none p-4 mx-4 pr-8 bg-payline-white border-payline-disabled flex flex-row items-center justify-between w-full"
          key={index}>
          <p className="text-left text-payline-black w-max">
            {itemDetailsText}
          </p>
          <p className="md:ml-10 text-right w-max">{priceDetails}</p>
        </div>,
      );
    });
  }
  return <div className="divide-y-2 md:mt-6 ">{accordionItems}</div>;
};

export default AccordionItem;
