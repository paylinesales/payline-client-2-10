import { gql } from "@apollo/client";

const websiteCopyQuery = gql`
  query websiteCopyQuery {
    websiteCopy {
      faqSection {
        faqs {
          question
          fieldGroupName
          answer
        }
      }
      calculatorSection {
        fieldGroupName
        sectionTitle {
          fieldGroupName
          highlight
          subtitle
          title
        }
        onlineData {
          label
        }
        inPersonData {
          label
        }
      }
    }
  }
`;

export default websiteCopyQuery;
