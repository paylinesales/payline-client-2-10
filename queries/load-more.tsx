import { gql } from "@apollo/client";

const loadMoreQuery = gql`
  query loadMoreQuery($id: ID!, $search: String!, $cursor: String!) {
    category(id: $id) {
      posts(first: 10, where: { search: $search }, after: $cursor) {
        pageInfo {
          startCursor
        }
        nodes {
          id
        }
      }
    }
  }
`;

export default loadMoreQuery;
