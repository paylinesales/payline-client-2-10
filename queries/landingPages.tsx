import { gql } from "@apollo/client";

const landingPageQuery = gql`
  query landingPageQuery($title: String!) {
    pages(where: { title: $title }) {
      edges {
        node {
          title
          slug
          heroSection {
            fieldGroupName
            headline {
              leadingText
              highlightedText
            }
            backgroundImage {
              sourceUrl
              title
            }
          }
          revenueSection {
            sectionText {
              sectionHeadline {
                headlineRemainder
                highlightedWord
              }
              subheadline
            }
            paymentOptionsLabel
            bulletPoints {
              bulletText
            }
            sectionImage {
              sourceUrl
              slug
              seo {
                metaKeywords
                metaDesc
                fullHead
                focuskw
                cornerstone
                canonical
                metaRobotsNofollow
                metaRobotsNoindex
                opengraphAuthor
                opengraphDescription
                opengraphModifiedTime
                opengraphTitle
                opengraphPublisher
                opengraphPublishedTime
                opengraphSiteName
                opengraphType
                opengraphUrl
                readingTime
                schema {
                  articleType
                  pageType
                  raw
                }
                title
                twitterDescription
                twitterImage {
                  title
                  sourceUrl
                  slug
                  sizes
                  uri
                }
                twitterTitle
                breadcrumbs {
                  text
                  url
                }
              }
              title
            }
          }
          getInTouchSection {
            accordions {
              accordionText
              label
              fieldGroupName
            }
            salesTeamFlow {
              callToAction
              fieldGroupName
              phoneCall {
                label
                phoneNumber
                fieldGroupName
              }
              salesTeamFlowHeadline {
                fieldGroupName
                firstHighlightedText
                headlineStartNonHighlightedText
                secondHighlightedText
                secondNonHighlightedText
              }
              salesTeamImages {
                fieldGroupName
                salesPersonImage {
                  altText
                  description(format: RAW)
                  sourceUrl
                  title(format: RAW)
                }
              }
              salesTeamLabel
              salesTeamStatus
            }
            fieldGroupName
            sectionTitle {
              highlightedText
              fieldGroupName
              nonHighlightedText
            }
          }
        }
      }
    }
    websiteCopy {
      logoBar {
        logoGroup {
          logo {
            sourceUrl
          }
        }
      }
    }
  }
`;

export default landingPageQuery;
